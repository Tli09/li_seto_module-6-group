from django.conf.urls import patterns, include, url
from cal.api import UserResource, CalResource, EventResource
from tastypie.api import Api
 
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
 
v1_api = Api(api_name='v1')
v1_api.register(UserResource())
v1_api.register(CalResource())

# RESTful setup:
user_resource = UserResource()
cal_resource = CalResource()
event_resource = EventResource()
 
urlpatterns = patterns('',
    # Admin page
    url(r'^admin/', include(admin.site.urls)),
 
    # Templates
    url(r'^', include('cal.urls')),
    
    #(r'^api/', include(v1_api.urls)),
 
    # RESTful URLs
    url(r'^', include(user_resource.urls)),
    url(r'^', include(cal_resource.urls)),
    url(r'^', include(event_resource.urls)),
    
    url(r'^accounts/login/$',  'useradmin.views.login'),
    url(r'^accounts/auth/$',  'useradmin.views.auth_view'),    
    url(r'^accounts/logout/$', 'useradmin.views.logout'),
    url(r'^accounts/loggedin/$', 'useradmin.views.loggedin'),
    #url(r'^', 'cal.views.loggedin'),
    url(r'^accounts/invalid/$', 'useradmin.views.invalid_login'),
    url(r'^accounts/register/$', 'useradmin.views.register_user'),
    url(r'^accounts/register_success/$', 'useradmin.views.register_success'),
)

