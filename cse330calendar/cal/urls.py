from django.conf.urls import patterns, url
from cal import views

 
urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^user/trylogin/$',  views.auth_view)
)