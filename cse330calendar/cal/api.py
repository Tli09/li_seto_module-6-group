from tastypie.resources import ModelResource
from tastypie.paginator import Paginator
from cal.models import Cal, Event
from tastypie.fields import ToOneField
from tastypie.authorization import Authorization
from django.contrib.auth.models import User
from tastypie import fields
from tastypie.authentication import BasicAuthentication
from auth import *

class UserResource(ModelResource):
	class Meta:
		queryset = User.objects.all()
		resource_name = 'auth/user'
		excludes = ['email', 'password', 'is_superuser']
		#authentication = BasicAuthentication()
		#authorization=UserObjectsOnlyAuthorization()
 
class CalResource(ModelResource):
	#user = fields.ForeignKey(UserResource, 'user')
	cid = ToOneField(UserResource, "user", full=True)
	class Meta:
		queryset = Cal.objects.all()
		paginator_class = Paginator
		authorization=CalAuth()
		
		filtering = {
		'user': ALL_WITH_RELATIONS,
		}
 
 
class EventResource(ModelResource):
	cid = ToOneField(CalResource, "cal", full=True)
	class Meta:
		queryset = Event.objects.all()
		paginator_class = Paginator
		always_return_data = True
		#authorization= Authorization()
		authorization=EventAuth() 
	def dehydrate_cid(self, bundle):
		return bundle.obj.cal.id
	def hydrate_cid(self, bundle):
		bundle.data["cid"] = "/cal/%d/" % bundle.data["cid"]
		return bundle
	def hydrate_id(self, bundle):
		if bundle.data["id"] == 0:
			bundle.data["id"] = None
		return bundle